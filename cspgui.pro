TEMPLATE = app

QT += qml quick widgets

CONFIG += link_pkgconfig

PKGCONFIG += ogdf

SOURCE_DIR = src

HEADERS += $${SOURCE_DIR}/utils.hpp \
    src/graph.hpp \
    src/logger.hpp \
    src/nodemodel.hpp \
    src/edgemodel.hpp \
    src/node.hpp \
    src/nodeiterator.hpp \
    src/edge.hpp
SOURCES += $${SOURCE_DIR}/main.cpp \
    src/graph.cpp \
    src/logger.cpp \
    src/nodemodel.cpp \
    src/edgemodel.cpp \
    src/node.cpp \
    src/nodeiterator.cpp \
    src/edge.cpp

RESOURCES += $${SOURCE_DIR}/qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

OTHER_FILES +=
