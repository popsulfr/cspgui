#ifndef NODEMODEL_HPP
#define NODEMODEL_HPP

#include <QAbstractListModel>

#include <ogdf/basic/NodeArray.h>
#include <ogdf/basic/GraphObserver.h>

#include <QMetaType>
#include <QList>

#include "nodeiterator.hpp"

namespace cspgui {

class Graph;
class Node;

class NodeModel : public QAbstractListModel, public ogdf::GraphObserver
{
    Q_OBJECT
    Q_PROPERTY(int count READ count NOTIFY countChanged)
public:
    enum NodeRoles {
        IndexRole=Qt::UserRole+1,
        NodeRole,
    };

    explicit NodeModel(Graph *graph);

    // Model
    Graph* getGraph() const;
    int count() const{return _nodes.size();}
    ogdf::node getNodeByIndex(int index);

    // QAbstractListModel
    QHash<int, QByteArray> roleNames() const{return _roles;}
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    Qt::ItemFlags flags(const QModelIndex &index) const;

    // ogdf::GraphObserver
    virtual void cleared();
    virtual void edgeAdded(ogdf::edge e);
    virtual void edgeDeleted(ogdf::edge e);
    virtual void nodeAdded(ogdf::node v);
    virtual void nodeDeleted(ogdf::node v);
    virtual void reInit();

    // QML
    Q_INVOKABLE QObject* addNode(qreal x,qreal y);
    Q_INVOKABLE void removeNode(QObject* node);

signals:
    void countChanged(int count);

public slots:

protected:
    QHash<int,QByteArray> _roles;
    QList<ogdf::node> _nodes;
};
}

#endif // NODEMODEL_HPP
