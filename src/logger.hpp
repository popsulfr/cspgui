#ifndef LOGGER_HPP
#define LOGGER_HPP

#include <QObject>
#include <QString>
#include <QtQml>

#define TIMESTAMP_FORMAT "yyyy/MM/dd hh:mm:ss"

namespace cspgui {

class Logger : public QObject
{
    Q_OBJECT
public:
    explicit Logger(QObject *parent = 0):QObject(parent){}
    Q_INVOKABLE void log(const QString& string) const;
    Q_INVOKABLE void debug(const QString& string) const;
    Q_INVOKABLE void info(const QString& string) const;
    Q_INVOKABLE void warn(const QString& string) const;
    Q_INVOKABLE void error(const QString& string) const;
    Q_INVOKABLE QString logEntryMaker(const QString& string) const;

signals:
    void logLogged(const QString& string) const;
    void debugLogged(const QString& string) const;
    void infoLogged(const QString& string) const;
    void warnLogged(const QString& string) const;
    void errorLogged(const QString& string) const;
protected:
    void logEntryCreator(QString& cont, const QString& string) const;
    void appendTimestamp(QString& string,const QString& format) const;
};

inline static QObject *logger_singletontype_provider(QQmlEngine *engine,QJSEngine *scriptEngine){
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    return new Logger();
}
}

#endif // LOGGER_HPP
