#include "edgemodel.hpp"
#include "node.hpp"
#include "edge.hpp"

#include <QDebug>

using namespace cspgui;

EdgeModel::EdgeModel(Graph *graph):QAbstractListModel(graph),ogdf::GraphObserver(graph->getOGDFGraph()){
    _roles[EdgeRole]="edge";
}

QVariant EdgeModel::data(const QModelIndex &index, int role) const{
    if(!index.isValid()){
        return false;
    }
    switch(role){
        case EdgeRole:
            return qVariantFromValue(new Edge(getGraph(),_edges.at(index.row())));
        default:
            return QVariant();
    }
}

bool EdgeModel::setData(const QModelIndex &index, const QVariant &value, int role){
    Q_UNUSED(value)
    Q_UNUSED(role)
    if(!index.isValid()){
        return false;
    }
    emit dataChanged(index,index);
    return true;
}

Graph* EdgeModel::getGraph() const{
    return static_cast<Graph*>(QObject::parent());
}

int EdgeModel::count() const{
    return _edges.size();
}

Qt::ItemFlags EdgeModel::flags(const QModelIndex &index) const{
    return QAbstractListModel::flags(index) | Qt::ItemIsEditable;
}

int EdgeModel::rowCount(const QModelIndex &parent) const{
    Q_UNUSED(parent)
    return count();
}

void EdgeModel::cleared(){
    beginResetModel();
    _edges.clear();
    endResetModel();
    emit countChanged(count());
}

void EdgeModel::edgeAdded(ogdf::edge e){
    int c=count();
    beginInsertRows(QModelIndex(),c,c);
    _edges.append(e);
    endInsertRows();
    emit countChanged(c+1);
}

void EdgeModel::edgeDeleted(ogdf::edge e){
    int index=_edges.indexOf(e);
    beginRemoveRows(QModelIndex(),index,index);
    _edges.removeAt(index);
    endRemoveRows();
    emit countChanged(count());
}

void EdgeModel::nodeAdded(ogdf::node){}
void EdgeModel::nodeDeleted(ogdf::node){}
void EdgeModel::reInit(){
    beginResetModel();
    _edges.clear();
    ogdf::edge e;
    forall_edges(e,*(getGraph()->getOGDFGraph())){
        _edges.append(e);
    }
    endResetModel();
    emit countChanged(count());
}

QObject* EdgeModel::addEdge(QObject* sourceNode,QObject* targetNode){
    Edge* edge=new Edge(getGraph(),getGraph()->getOGDFGraph()->newEdge(
                            static_cast<Node*>(sourceNode)->getOGDFNode(),
                            static_cast<Node*>(targetNode)->getOGDFNode()));
    return edge;
}

void EdgeModel::removeEdge(QObject* edge){
    getGraph()->getOGDFGraph()->delEdge(static_cast<Edge*>(edge)->getOGDFEdge());
}
