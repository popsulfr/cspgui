#ifndef EDGE_HPP
#define EDGE_HPP

#include <QObject>
#include <ogdf/basic/GraphAttributes.h>

#include "graph.hpp"
#include "node.hpp"

namespace cspgui {

class Edge : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int index READ getIndex CONSTANT)
    Q_PROPERTY(QObject* sourceNode READ getSourceNode CONSTANT)
    Q_PROPERTY(QObject* targetNode READ getTargetNode CONSTANT)
public:
    explicit Edge(Graph *parent,ogdf::edge edge):
        QObject(parent),
        _edge(edge){}

    int getIndex() const{return _edge->index();}
    QObject* getSourceNode() const{return new Node(getGraph(),_edge->source());}
    QObject* getTargetNode() const{return new Node(getGraph(),_edge->target());}
    ogdf::edge getOGDFEdge() const{return _edge;}

signals:

public slots:

protected:
    Graph* getGraph() const{
        return static_cast<Graph*>(QObject::parent());
    }

    ogdf::GraphAttributes* getGraphAttributes() const{
        return getGraph()->getOGDFGraphAttributes();
    }

    ogdf::edge _edge;
};
}

#endif // EDGE_HPP
