#ifndef NODE_HPP
#define NODE_HPP

#include "graph.hpp"

#include <QString>
#include <QObject>
#include <QDebug>

#include <ogdf/basic/GraphAttributes.h>

namespace cspgui {

class Node : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int index READ getIndex CONSTANT)
    Q_PROPERTY(QString label READ getLabel WRITE setLabel NOTIFY labelChanged)
    Q_PROPERTY(qreal x READ getX WRITE setX NOTIFY xChanged)
    Q_PROPERTY(qreal y READ getY WRITE setY NOTIFY yChanged)
    Q_PROPERTY(qreal z READ getZ WRITE setZ NOTIFY zChanged)
    Q_PROPERTY(qreal width READ getWidth WRITE setWidth NOTIFY widthChanged)
    Q_PROPERTY(qreal height READ getHeight WRITE setHeight NOTIFY heightChanged)
public:
    explicit Node(Graph *parent,ogdf::node node):
        QObject(parent),
        _node(node){}

    ogdf::node getOGDFNode() const{return _node;}

    // QML
    int getIndex() const{return _node->index();}
    QString getLabel() const{return QString::fromStdString(getGraphAttributes()->label(_node));}
    void setLabel(const QString& label){getGraphAttributes()->label(_node)=label.toStdString(); emit labelChanged(label);}
    qreal getX() const{
        return getGraphAttributes()->x(_node);
    }
    void setX(qreal x){
        getGraphAttributes()->x(_node)=x; emit xChanged(x);}
    qreal getY() const{
        return getGraphAttributes()->y(_node);}
    void setY(qreal y){getGraphAttributes()->y(_node)=y; emit yChanged(y);}
    qreal getZ() const{return getGraphAttributes()->z(_node);}
    void setZ(qreal z){getGraphAttributes()->z(_node)=z; emit zChanged(z);}
    qreal getWidth() const{
        return getGraphAttributes()->width(_node);}
    void setWidth(qreal width){
        getGraphAttributes()->width(_node)=width; emit widthChanged(width);}
    qreal getHeight() const{
        return getGraphAttributes()->height(_node);}
    void setHeight(qreal height){getGraphAttributes()->height(_node)=height; emit heightChanged(height);}

    bool operator==(const Node& b) const{return getIndex()==b.getIndex();}
signals:
    void labelChanged(const QString& label);
    void xChanged(qreal x);
    void yChanged(qreal y);
    void zChanged(qreal z);
    void widthChanged(qreal width);
    void heightChanged(qreal height);

public slots:

protected:
    Graph* getGraph() const{
        return static_cast<Graph*>(QObject::parent());
    }
    ogdf::GraphAttributes* getGraphAttributes() const{
        return getGraph()->getOGDFGraphAttributes();
    }
    ogdf::node _node;
};
}
#endif // NODE_HPP
