#include <QDebug>
#include <QString>
#include <QDateTime>

#include "logger.hpp"

#define LOG_BODY(string,stream,listenerfunc) QString message; \
    logEntryCreator(message,string); \
    stream << message; \
    emit listenerfunc(message)

using namespace cspgui;

void Logger::log(const QString& string) const{
    LOG_BODY(string,qDebug(),logLogged);
}
void Logger::debug(const QString& string) const{
    LOG_BODY(string,qDebug(),debugLogged);
}
void Logger::info(const QString& string) const{
    LOG_BODY(string,qDebug(),infoLogged);
}
void Logger::warn(const QString& string) const{
    LOG_BODY(string,qWarning(),warnLogged);
}
void Logger::error(const QString& string) const{
    LOG_BODY(string,qCritical(),errorLogged);
}

void Logger::logEntryCreator(QString& message, const QString& string) const{
    appendTimestamp(message,TIMESTAMP_FORMAT);
    message.append(" ");
    message.append(string);
}

void Logger::appendTimestamp(QString& string,const QString& format) const{
    string.append("[");
    string.append(QDateTime::currentDateTime().toString(format));
    string.append("]");
}

QString Logger::logEntryMaker(const QString& string) const{
    QString message;
    logEntryCreator(message,string);
    return message;
}
