import QtQuick 2.2
import "GraphView.js" as GraphViewFuncs
import cspgui 1.0
import QtQuick.Controls 1.1

Item {
    // Section of interest to the user
    id: graphview
    property QtObject graphmodel: Graph {}
    property Component edgeDelegate: Canvas {
        id: canvasedge
        property color color: '#ffffff'
        property int minX: Math.min(model.sourceNode.x, model.targetNode.x)
        property int minY: Math.min(model.sourceNode.y, model.targetNode.y)
        property int maxX: Math.max(model.sourceNode.x, model.targetNode.x)
        property int maxY: Math.max(model.sourceNode.y, model.targetNode.y)
        x: Math.min(model.sourceNode.x, model.targetNode.x)
        y: Math.min(model.sourceNode.y, model.targetNode.y)
        width: Math.max(model.sourceNode.x, model.targetNode.x)-Math.min(model.sourceNode.x, model.targetNode.x)
        height: Math.max(model.sourceNode.y, model.targetNode.y)-Math.min(model.sourceNode.y, model.targetNode.y)
        antialiasing: true
        onMinXChanged: {
            console.log("minX changed");
        }

        onPaint: {
            console.log("MinX: "+minX+" MinY: "+minY+" MaxX: "+maxX+" MaxY: "+maxY+" Width: "+width+" Height: "+height);
            var ctx = getContext("2d")
            ctx.strokeStyle = "red"
            ctx.beginPath()
            ctx.moveTo(0,0)
            ctx.lineTo(width,height)
            ctx.stroke()
        }
    }

    property Component nodeDelegate: Rectangle {
        id: rectNode
        readonly property var mymodel: model
        width: childrenRect.width
        height: childrenRect.height
        color: "white"
        radius: 6
        border.width: 1
        border.color: (graphview.selectedindex===mymodel.index)?"red":"black";
        focus: (graphview.selectedindex===mymodel.index)

        Menu {
            id: contextmenunode
            MenuItem {
                text: qsTr("Delete node")
                onTriggered: {
                    if(graphview.selectedindex>-1){
                        graphview.graphmodel.nodes.removeNode(mymodel);
                    }
                }
            }
        }

        MouseArea {
            width: childrenRect.width+16
            height: childrenRect.height+8
            acceptedButtons: Qt.LeftButton | Qt.RightButton
            property bool dragging: false
            onClicked: {
                graphview.selectedindex=mymodel.index;
                if(mouse.button===Qt.RightButton){
                    contextmenunode.popup();
                }
            }

            onPressed: {
                graphview.selectedindex=mymodel.index;
                parent.z=graphview.graphmodel.msecsEllapsed;
                dragging=true;
            }
            onReleased: {
                dragging=false;
            }
            onPositionChanged: {
                if(dragging){
                    model.x+=mouse.x-(parent.width/2);
                    model.y+=mouse.y-(parent.height/2);
                }
            }
            TextInput {
                id: varname
                x: 8
                y: 4
                font.pixelSize: 12
                text: mymodel.label
            }
        }
    }

    // internal stuff
    x: childrenRect.x
    y: childrenRect.y
    width: childrenRect.width
    height: childrenRect.height
    property int selectedindex: -1

    signal removeNode(int index);

    Menu {
        id: contextmenu
        MenuItem {
            text: qsTr("Create node")
            onTriggered: {
                graphview.selectedindex=graphview.graphmodel.nodes.addNode(mainmousearea.mouseX,mainmousearea.mouseY).index;
            }
        }
    }

    MouseArea {
        id: mainmousearea
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onClicked: {
            graphview.selectedindex=-1;
            if(mouse.button==Qt.RightButton){
                contextmenu.popup();
            }
        }
    }

    Repeater {
        model: graphview.graphmodel.edges
        delegate: Item {
            readonly property var itemModel: model.edge
            id: edgeItem
            x: 0
            y: 0
            width: childrenRect.width
            height: childrenRect.height

            Loader {
                property var model: edgeItem.itemModel
                sourceComponent: graphview.edgeDelegate
                Component.onDestruction: {
                    model={sourceNode: {x:0,y:0}, targetNode: {x:0,y:0}};
                }
            }
        }
    }

    Repeater {
        model: graphview.graphmodel.nodes
        delegate: Item {
            readonly property var itemModel: model.node
            id: nodeItem
            x: model.node.x - (model.node.width/2)
            y: model.node.y - (model.node.height/2)
            z: model.node.z
            width: childrenRect.width
            height: childrenRect.height
            Binding {
                target: model.node
                property: "width"
                value: nodeItem.width
            }
            Binding {
                target: model.node
                property: "height"
                value: nodeItem.height
            }
            Binding {
                target: model.node
                property: "z"
                value: nodeItem.z
            }
            onXChanged: {
                console.log("index: "+model.node.index+" X: "+x);
            }

            Loader {
                property var model: nodeItem.itemModel
                sourceComponent: graphview.nodeDelegate
                Component.onDestruction: {
                    model={index: -1,label: "",x: 0,y: 0,z: 0,width: 0,height: 0};
                }
            }
        }
    }
}
