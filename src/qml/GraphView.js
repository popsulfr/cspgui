function backgroundColor(context,color,width,height){
    context.fillStyle=color;
    context.fillRect(0,0,width,height);
}

function drawNode(context,x,y){
    context.fillStyle='black';
    context.beginPath();
    context.arc(x, y, 20, 0, Math.PI*2, false);
    context.closePath();
    context.fill();
}
