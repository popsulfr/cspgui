import QtQuick 2.0
import cspgui 1.0
import QtQuick.Controls 1.1

Item {
    id: graphview
    property QtObject graphmodel: Graph {}
    width: childrenRect.width
    height: childrenRect.height

    property int selectedNodeCount: 0

    property var nodepair: [null,null]

    signal deselectAllNodes();
    onDeselectAllNodes:{
        selectedNodeCount=0;
        nodepair[0]=null;
        nodepair[1]=null;
    }

    signal removeNodes();

    Menu {
        id: contextmenu
        MenuItem {
            text: qsTr("Add node")
            onTriggered: {
                graphview.graphmodel.nodes.addNode(mainmousearea.mouseX,mainmousearea.mouseY);
            }
        }
        MenuItem {
            text: qsTr("Delete node")
            enabled: graphview.selectedNodeCount>0
            onTriggered: {
                graphview.removeNodes();
                graphview.deselectAllNodes();
            }
        }
        MenuItem {
            text: qsTr("Create edge")
            enabled: graphview.selectedNodeCount===2 &&
                     graphview.nodepair[0]!==null && graphview.nodepair[1]!==null
            onTriggered: {
                graphview.graphmodel.edges.addEdge(graphview.nodepair[0],graphview.nodepair[1]);
            }
        }
    }

    MouseArea {
        id: mainmousearea
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onClicked: {
            if(mouse.button==Qt.RightButton){
                contextmenu.popup();
            }
        }
    }

    signal nodePositionChanged(int nodeindex);

    Repeater {
        model: graphview.graphmodel.edges
        Canvas {
            id: edgeCanvas
            property color lineColor: "black"
            property int minX: Math.min(model.edge.sourceNode.x, model.edge.targetNode.x)
            property int minY: Math.min(model.edge.sourceNode.y, model.edge.targetNode.y)
            property int maxX: Math.max(model.edge.sourceNode.x, model.edge.targetNode.x)
            property int maxY: Math.max(model.edge.sourceNode.y, model.edge.targetNode.y)
            x: minX
            y: minY
            width: maxX-minX
            height: maxY-minY
            antialiasing: true

            Connections {
                target: graphview
                onNodePositionChanged: {
                    if(nodeindex===model.edge.sourceNode.index ||
                            nodeindex===model.edge.targetNode.index){
                        edgeCanvas.minX=Math.min(model.edge.sourceNode.x, model.edge.targetNode.x);
                        edgeCanvas.minY=Math.min(model.edge.sourceNode.y, model.edge.targetNode.y);
                        edgeCanvas.maxX=Math.max(model.edge.sourceNode.x, model.edge.targetNode.x);
                        edgeCanvas.maxY=Math.max(model.edge.sourceNode.y, model.edge.targetNode.y);
                        edgeCanvas.requestPaint();
                    }
                }
            }

            onPaint: {
                var ctx = getContext("2d")
                ctx.strokeStyle =lineColor;
                ctx.beginPath();
                ctx.moveTo(model.edge.sourceNode.x-minX,model.edge.sourceNode.y-minY);
                ctx.lineTo(model.edge.targetNode.x-minX,model.edge.targetNode.y-minY);
                ctx.stroke();
            }
        }
    }

    Repeater {
        model: graphview.graphmodel.nodes
        Rectangle {
            id: rectNode
            property color baseColor: "black"
            property color highlightColor: "red"
            property color backgroundColor: "white"

            property bool selected: false
            x: model.node.x - (model.node.width/2)
            y: model.node.y - (model.node.height/2)
            z: model.node.z
            width: childrenRect.width
            height: childrenRect.height
            color: backgroundColor
            radius: 6
            border.width: 1
            border.color: (selected)?highlightColor:baseColor
            Binding {
                target: model.node
                property: "width"
                value: width
            }
            Binding {
                target: model.node
                property: "height"
                value: height
            }
            Connections {
                target: graphview
                onDeselectAllNodes: {
                    rectNode.selected=false;
                }
                onRemoveNodes: {
                    if(rectNode.selected){
                        graphview.graphmodel.nodes.removeNode(model.node);
                    }
                }
            }

            MouseArea {
                property bool dragging: false
                width: childrenRect.width+16
                height: childrenRect.height+8
                acceptedButtons: Qt.LeftButton | Qt.RightButton

                onPressed: {
                    if(mouse.modifiers===Qt.ControlModifier){
                        if((rectNode.selected=!(rectNode.selected))){
                            if(graphview.selectedNodeCount<2)
                                graphview.nodepair[graphview.selectedNodeCount]=model.node;
                            ++graphview.selectedNodeCount;
                        }
                        else{
                            --graphview.selectedNodeCount;
                            if(graphview.selectedNodeCount<2)
                                graphview.nodepair[graphview.selectedNodeCount]=null;
                        }
                    }
                    else{
                        graphview.deselectAllNodes();
                        rectNode.selected=true;
                        ++graphview.selectedNodeCount;
                        graphview.nodepair[0]=model.node;
                    }
                    if(mouse.button===Qt.LeftButton) dragging=true;
                    else if(mouse.button===Qt.RightButton) contextmenu.popup();
                }
                onReleased: {
                    dragging=false;
                }
                onPositionChanged: {
                    if(dragging){
                        rectNode.x=(model.node.x+=mouse.x)-(model.node.width/2);
                        rectNode.y=(model.node.y+=mouse.y)-(model.node.height/2);
                        graphview.nodePositionChanged(model.index);
                    }
                }

                TextInput {
                    x: 8
                    y: 4
                    font.pixelSize: 12
                    text: model.node.label
                }
            }
        }
    }
}
