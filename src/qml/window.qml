import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.1

ApplicationWindow {
    id: mainapplicationwindow
    visible: true
    width: 640
    height: 480
    title: qsTr("CSP Graph-O-Matic")

    menuBar: MenuBar {
        Menu {
            title: qsTr("File")
            MenuItem {
                text: qsTr("New")
            }

            MenuItem {
                text: qsTr("Exit")
                onTriggered: Qt.quit();
            }
        }
    }

    SplitView {
        id: mainsplitview
        anchors.fill: parent
        orientation: Qt.Vertical

        GraphView2 {
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            Layout.fillHeight: true
        }

        TabView {
            id: bottomtabview
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            Layout.minimumHeight: 48
            Layout.maximumHeight: 128

            Tab {
                title: "Log"
                source: "LogView.qml"
            }

            Tab {
                title: "Console"
                TextArea {}
            }

            /*
            Component.onCompleted: {
                addTab("Log",Qt.createComponent("LogView.qml"));
                addTab("Console",tab2);
            }

            Component {
                id: tab2
                TextArea {}
            }
            */
        }
    }

    statusBar: StatusBar {
        Label {
            text:  qsTr("Hello")
        }
    }


}
