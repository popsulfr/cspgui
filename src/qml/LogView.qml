import QtQuick 2.2
import QtQuick.Controls 1.1
import cspgui 1.0

TextArea {
    id: mainlog
    wrapMode: TextEdit.Wrap
    textFormat: TextEdit.RichText
    text: "> "+Logger.logEntryMaker("<b>"+qsTr("Welcome aboard!")+"</b>")
    readOnly: true

    Connections {
        target: Logger
        onLogLogged: mainlog.append("> "+string);
        onDebugLogged: mainlog.append("> "+string);
        onInfoLogged: mainlog.append("> "+string);
        onWarnLogged: mainlog.append('> <font color="yellow">'+string+'</font>')
        onErrorLogged: mainlog.append('> <font color="red"><b>'+string+'</b></font>')
    }

    MouseArea {
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onClicked: {
            if(mouse.button==Qt.RightButton){
                contextmenu.popup();
            }
        }
    }

    Menu {
        id: contextmenu
        MenuItem {
            text: qsTr("Clear")
            onTriggered: {
                mainlog.remove(0,mainlog.length);
            }
        }
    }
}
