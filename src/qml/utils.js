function createLogEntry(string){
    return "> "+Qt.formatDateTime(new Date(),"[yyyy/MM/dd hh:mm:ss]")+" "+string;
}
