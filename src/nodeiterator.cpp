#include "nodeiterator.hpp"

using namespace cspgui;

void NodeIterator::nextNode(){
    if(_index<nodeCount()){
        ++_index;
        if(_currentNode){
            _currentNode=_currentNode->succ();
        }
    }
}

void NodeIterator::prevNode(){
    if(_index>0){
        --_index;
        if(_currentNode){
            _currentNode=_currentNode->pred();
        }
    }
}

void NodeIterator::resetToFirst(){
    _index=0;
    _currentNode=getGraph()->firstNode();
}

void NodeIterator::resetToLast(){
    _index=nodeCount();
    if(_index>0) --_index;
    _currentNode=getGraph()->lastNode();
}

ogdf::node NodeIterator::getNodeByIndex(int index){
    int count;
    if(index>=0 && index<(count=nodeCount())){
        if(index==_index){
            return _currentNode;
        }
        else if(index<_index){
            if((_index-index)<index){
                for(index=_index-index;index>0;--index,prevNode());
                return _currentNode;
            }
            else{
                resetToFirst();
                for(;index>0;--index,nextNode());
                return _currentNode;
            }
        }
        else{
            if((count-index)<(index-_index)){
                resetToLast();
                for(index-=count;index>0;--index,prevNode());
                return _currentNode;
            }
            else{
                for(index-=_index;index>0;--index,nextNode());
                return _currentNode;
            }
        }
    }
    return 0;
}
