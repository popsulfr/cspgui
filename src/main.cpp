#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QQmlEngine>

#include <iostream>

#include <ogdf/basic/Graph.h>
#include <ogdf/basic/GraphAttributes.h>

#include "utils.hpp"
#include "logger.hpp"
#include "nodemodel.hpp"
#include "edgemodel.hpp"
#include "node.hpp"
#include "edge.hpp"

#define LIBRARY_NAME "cspgui"

static void registerQmlTypes(){
    qmlRegisterSingletonType<cspgui::Logger>(LIBRARY_NAME,1,0,"Logger",cspgui::logger_singletontype_provider);
    qmlRegisterUncreatableType<cspgui::Node>(LIBRARY_NAME,1,0,"Node","Trying to create uncreatable type Node");
    qmlRegisterUncreatableType<cspgui::Edge>(LIBRARY_NAME,1,0,"Edge","Trying to create uncreatable type Edge");
    qmlRegisterUncreatableType<cspgui::NodeModel>(LIBRARY_NAME,1,0,"NodeModel","Trying to create uncreatable type NodeModel");
    qmlRegisterUncreatableType<cspgui::EdgeModel>(LIBRARY_NAME,1,0,"EdgeModel","Trying to create uncreatable type EdgeModel");
    qmlRegisterType<cspgui::Graph>(LIBRARY_NAME,1,0,"Graph");
}

int main(int argc, char *argv[])
{
    registerQmlTypes();
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:///qml/window.qml")));

    return app.exec();
}
