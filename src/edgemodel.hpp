#ifndef EDGEMODEL_HPP
#define EDGEMODEL_HPP

#include <QAbstractListModel>
#include <QHash>
#include <QList>

#include <ogdf/basic/GraphObserver.h>

namespace cspgui {

class Graph;

class EdgeModel : public QAbstractListModel, public ogdf::GraphObserver
{
    Q_OBJECT
    Q_PROPERTY(int count READ count NOTIFY countChanged)
public:
    enum EdgeRoles {
        EdgeRole=Qt::UserRole+1,
    };

    explicit EdgeModel(Graph *graph);

    // Model
    Graph* getGraph() const;

    // QML
    QHash<int, QByteArray> roleNames() const{
        return _roles;
    }

    int count() const;
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    Qt::ItemFlags flags(const QModelIndex &index) const;

    // ogdf::GraphObserver
    virtual void cleared();
    virtual void edgeAdded(ogdf::edge e);
    virtual void edgeDeleted(ogdf::edge e);
    virtual void nodeAdded(ogdf::node v);
    virtual void nodeDeleted(ogdf::node v);
    virtual void reInit();

    // QML
    Q_INVOKABLE QObject* addEdge(QObject* sourceNode,QObject* targetNode);
    Q_INVOKABLE void removeEdge(QObject* edge);

signals:
    void countChanged(int count);
public slots:

protected:
    QHash<int,QByteArray> _roles;
    QList<ogdf::edge> _edges;
};
}

#endif // EDGEMODEL_HPP
