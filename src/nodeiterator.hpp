#ifndef NODEITERATOR_HPP
#define NODEITERATOR_HPP

#include <ogdf/basic/GraphObserver.h>

namespace cspgui {

class NodeIterator : public ogdf::GraphObserver
{
public:
    NodeIterator(const ogdf::Graph* graph):
        ogdf::GraphObserver(graph),_index(0),_currentNode(graph->firstNode()){}

    ogdf::node getNodeByIndex(int index);
    int nodeCount() const{return getGraph()->numberOfNodes();}

    // GraphObserver
    virtual void cleared(){resetToFirst();}
    virtual void edgeAdded(ogdf::edge){resetToFirst();}
    virtual void edgeDeleted(ogdf::edge){resetToFirst();}
    virtual void nodeAdded(ogdf::node){resetToFirst();}
    virtual void nodeDeleted(ogdf::node){resetToFirst();}
    virtual void reInit(){resetToFirst();}

protected:
    void nextNode();
    void prevNode();
    void resetToFirst();
    void resetToLast();
    int _index;
    ogdf::node _currentNode;
};
}

#endif // NODEITERATOR_HPP
