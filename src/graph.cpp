#include "graph.hpp"

#include <ogdf/basic/GraphAttributes.h>

using namespace cspgui;

Graph::Graph(QObject *parent):
    QObject(parent),
    _graphAttributes(_graph,
                     ogdf::GraphAttributes::nodeGraphics|
                     ogdf::GraphAttributes::edgeGraphics|
                     ogdf::GraphAttributes::edgeLabel|
                     ogdf::GraphAttributes::nodeLabel|
                     ogdf::GraphAttributes::threeD),
    _nodeModel(this),
    _edgeModel(this),
    _birth(QDateTime::currentDateTime()){
    /*
    ogdf::node n1=_graph.newNode();
    _graphAttributes.x(n1)=100;
    _graphAttributes.y(n1)=100;
    _graphAttributes.label(n1)="V1";
    ogdf::node n2=_graph.newNode();
    _graphAttributes.x(n2)=200;
    _graphAttributes.y(n2)=150;
    _graphAttributes.label(n2)="V2";
    ogdf::edge e=_graph.newEdge(n1,n2);
    */
}
