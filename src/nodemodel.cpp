#include "nodemodel.hpp"
#include "graph.hpp"
#include "node.hpp"

#include <ogdf/basic/Graph.h>

#include <QDebug>
#include <QtGlobal>

using namespace cspgui;

NodeModel::NodeModel(Graph *graph):
    QAbstractListModel(graph),
    ogdf::GraphObserver(graph->getOGDFGraph()){
    _roles[IndexRole]="index";
    _roles[NodeRole]="node";
}

QVariant NodeModel::data(const QModelIndex &index, int role) const{
    if(!index.isValid()){
        return QVariant();
    }
    switch(role){
        case IndexRole:
        return QVariant(index.row());
        case NodeRole:
        return qVariantFromValue(new Node(getGraph(),_nodes.at(index.row())));
        default:
            return QVariant();
    }
}

bool NodeModel::setData(const QModelIndex &index, const QVariant &value, int role){
    Q_UNUSED(value)
    Q_UNUSED(role)
    if(!index.isValid()){
        return false;
    }
    emit dataChanged(index,index);
    return true;
}

Graph* NodeModel::getGraph() const{
    return static_cast<Graph*>(QObject::parent());
}

Qt::ItemFlags NodeModel::flags(const QModelIndex &index) const{
    return QAbstractListModel::flags(index) | Qt::ItemIsEditable;
}

int NodeModel::rowCount(const QModelIndex &parent) const{
    Q_UNUSED(parent)
    return count();
}

QObject* NodeModel::addNode(qreal x,qreal y){
    reregister(0);
    Node* node=new Node(getGraph(),getGraph()->getOGDFGraph()->newNode());
    node->setX(x);
    node->setY(y);
    node->setZ(getGraph()->getMsecsEllapsed());
    node->setLabel("V"+QString::number(node->getOGDFNode()->index()));
    reregister(getGraph()->getOGDFGraph());
    nodeAdded(node->getOGDFNode());
    return node;
}

void NodeModel::removeNode(QObject* node){
    Node* n=static_cast<Node*>(node);
    qDebug() << n->getLabel();
    getGraph()->getOGDFGraph()->delNode(static_cast<Node*>(node)->getOGDFNode());
}

void NodeModel::cleared(){
    beginResetModel();
    _nodes.clear();
    endResetModel();
    emit countChanged(count());
}

void NodeModel::edgeAdded(ogdf::edge){}
void NodeModel::edgeDeleted(ogdf::edge){}

void NodeModel::nodeAdded(ogdf::node v){
    int c=count();
    beginInsertRows(QModelIndex(),c,c);
    _nodes.append(v);
    endInsertRows();
    emit countChanged(c+1);
}

void NodeModel::nodeDeleted(ogdf::node v){
    int index=_nodes.indexOf(v);
    beginRemoveRows(QModelIndex(),index,index);
    _nodes.removeAt(index);
    endRemoveRows();
    emit countChanged(count());
}

void NodeModel::reInit(){
    beginResetModel();
    _nodes.clear();
    ogdf::node v;
    forall_nodes(v,*(getGraph()->getOGDFGraph())){
        _nodes.append(v);
    }
    endResetModel();
    emit countChanged(count());
}
