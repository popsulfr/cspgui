#ifndef GRAPH_HPP
#define GRAPH_HPP

#include <QObject>
#include <QString>
#include <QDateTime>

#include <ogdf/basic/GraphAttributes.h>

#include "nodemodel.hpp"
#include "edgemodel.hpp"

namespace cspgui {

class Graph : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ getName WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QObject* nodes READ getNodes CONSTANT)
    Q_PROPERTY(QObject* edges READ getEdges CONSTANT)
    Q_PROPERTY(qreal msecsEllapsed READ getMsecsEllapsed)
public:
    explicit Graph(QObject *parent = 0);

    // Model
    ogdf::GraphAttributes* getOGDFGraphAttributes(){return &_graphAttributes;}

    ogdf::Graph* getOGDFGraph(){return &_graph;}

    // QML
    const QString& getName() const{return _name;}
    void setName(const QString& name){_name=name; emit nameChanged(_name);}
    NodeModel* getNodes(){return &_nodeModel;}
    EdgeModel* getEdges(){return &_edgeModel;}
    qreal getMsecsEllapsed() const{return _birth.msecsTo(QDateTime::currentDateTime())/1000.;}

    Q_INVOKABLE void clear(){_graph.clear();}

signals:
    void nameChanged(const QString& name);

public slots:

protected:
    QString _name;
    ogdf::Graph _graph;
    ogdf::GraphAttributes _graphAttributes;

    NodeModel _nodeModel;
    EdgeModel _edgeModel;

    QDateTime _birth;
};
}

#endif // GRAPH_HPP
